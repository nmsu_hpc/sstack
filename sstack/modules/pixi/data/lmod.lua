help([[For help contact your site administrators.]])
whatis([[Name: pixi ]])
whatis([[Version: Latest]])
whatis([[Target: x86_64]])
whatis([[Short description: pixi is a fast software package manager built on top of the existing conda ecosystem. Spins up development environments quickly on Windows, macOS and Linux. Automatic lockfiles produce reproducible environments across operating systems (without Docker!). pixi supports Python, R, C/C++, Rust, Ruby, and many other languages. ]])

family("pixi")
conflict("conda", "micromamba", "spack", "easybuild")

local base = ""

-- sh,dash,bash,zsh,csh,tcsh,ksh
local myShell = myShellName()

setenv("PIXI_ROOT",base)
prepend_path("MODULEPATH",pathJoin(base,'modules'))

-- Source Environment Setup Scripts
if (myShell == "bash") then
    -- Load/Unload Commands
    if (mode() == "load") then
        local cmd = 'source ' .. pathJoin(base,'share/setup-env.sh')
        execute{cmd=cmd, modeA={"load"}}
    elseif (mode() == "unload") then
        local cmd = 'source ' .. pathJoin(base,'share/unsetup-env.sh')
        execute{cmd=cmd, modeA={"unload"}}
    end
end
