#!/usr/bin/env bash

# Stop On Error
set -e

# Get Absolute Path To This Script
script_root="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# Get Arguments
install_dir="$1"

# Set Trap to cleanup on exit
function cleanup(){
	ecode=$?
	if [ $ecode -ne 0 ]; then
		rm -rf "$install_dir"
	fi
	exit $ecode
}
trap cleanup EXIT

# Ensure Install Directory
mkdir -p "$install_dir"

# Create Additional Directories
mkdir -p "$install_dir/bin"
mkdir -p "$install_dir/envs"
mkdir -p "$install_dir/modules"
mkdir -p "$install_dir/share"

# Download Latest Binary
PIXI_HOME="$install_dir"
PIXI_NO_PATH_UPDATE=1
export PIXI_HOME PIXI_NO_PATH_UPDATE
curl -fsSL https://pixi.sh/install.sh | bash

# Copy Share Folder/Files
cp -r "${script_root}/share"/. "${install_dir}/share"/
