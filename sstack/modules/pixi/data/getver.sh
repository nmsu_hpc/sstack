#!/usr/bin/env bash

# Stop On Error
set -e

# Get Arguments
pixi_root="$1"

# Setup ENV
# shellcheck disable=SC1091
source "$pixi_root/share/setup-env.sh" &>/dev/null

# Current Version
pixi --version | awk '{print $2}'
