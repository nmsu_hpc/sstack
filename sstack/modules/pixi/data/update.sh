#!/usr/bin/env bash

# Stop On Error
set -e

# Get Arguments
pixi_root="$1"

# Update Pixi using Pixi
"$pixi_root/bin/pixi" self-update --force
