#!/usr/bin/env bash

SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
STACK_ROOT="$(dirname "$SCRIPT_DIR")"
bdir="$STACK_ROOT/bin"

# Setup Path
if ! [[ "$PATH" =~ (^|:)"${bdir}"(:|$) ]]; then
	PATH="${bdir}:$PATH"
	export PATH
fi

# Setup Shell Completion
eval "$(pixi completion --shell bash)"

# Cleanup This Scripts Vars
unset SCRIPT_DIR
unset STACK_ROOT
unset bdir
