#!/usr/bin/env bash

SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
STACK_ROOT="$(dirname "$SCRIPT_DIR")"

# Cleanup Path
PATH="$(echo "$PATH" | tr ':' '\n' | grep -Exv "^\$|^${STACK_ROOT}\$|^${STACK_ROOT}/.*\$" | paste -sd ':' -)"
export PATH

# Cleanup Env
# shellcheck disable=SC2046
{
	# Variables
	unset -v $(compgen -v | grep -Ei '^pixi')

	# Functions
	unset -f $(compgen -A 'function' | grep -Ei '^pixi|^_pixi|^__pixi')
}

# Cleanup This Script's Vars
unset SCRIPT_DIR
unset STACK_ROOT
