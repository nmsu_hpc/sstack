import os
from sstack.modules.helpers import *

mod_root = os.path.dirname(__file__)

def install(stack: sstack_stack):

    # Set Data Config File Root
    data_root = os.path.join(mod_root,"data")

    # Install Stack
    if os.path.exists(stack.path):
        print("Failed: instance already exists.")
        exit(1)
    else:
        # Run Installation
        script = os.path.join(data_root, 'install.sh')
        os.system('bash "{}" "{}"'.format(script, stack.path))

def generate_module(stack: sstack_stack):

    # Set Data Config File Root
    data_root = os.path.join(mod_root,"data")

    # Ensure Module Directory Exists
    parent_dir = os.path.dirname(stack.module)
    if not os.path.exists(parent_dir):
        os.mkdir(parent_dir)

    # Open Source and Destination Files
    src = open(os.path.join(data_root,"lmod.lua"), "r")
    destFile = open(stack.module, "w")

    # Write source to destination and replace lines as needed
    for line in src:
        if "local base" in line:
            destFile.write("local base = \"{0}\"\n".format(stack.path))
        elif "Version" in line:
            destFile.write("whatis([[Version: {}]])\n".format(stack.version))
        else:
            destFile.write(line)

    # Close Open Files
    src.close()
    destFile.close()

def update(stack: sstack_stack) -> None:
    print('Stack type "custom" does not support upgrades as it is meant for hand built software.')

def get_version(stack: sstack_stack) -> str:
    return stack.version
