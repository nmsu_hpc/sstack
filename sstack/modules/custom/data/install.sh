#!/usr/bin/env bash

# Stop On Error
set -e

# Get Arguments
install_dir="$1"

# Set Trap to cleanup on exit
function cleanup(){
	ecode=$?
	if [ $ecode -ne 0 ]; then
		rm -rf "$install_dir"
	fi
	exit $ecode
}
trap cleanup EXIT

# Create Default Directories
mkdir -p "${install_dir}"
mkdir -p "${install_dir}/packages"
mkdir -p "${install_dir}/sources"
mkdir -p "${install_dir}/builds"
mkdir -p "${install_dir}/modules"
