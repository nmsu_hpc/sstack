#!/usr/bin/env bash

# Stop On Error
set -e

# Get Arguments
install_dir="$1"

# change to install dir
cd "$install_dir"

# Get Branch Update From Git
git pull --autostash

# Rerun Make
make clean-envs
make build-prod
