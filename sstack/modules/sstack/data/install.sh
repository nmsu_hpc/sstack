#!/usr/bin/env bash

# Stop On Error
set -e

# Get Arguments
install_dir="$1"

# Set Trap to cleanup on exit
function cleanup(){
	ecode=$?
	if [ $ecode -ne 0 ]; then
		rm -rf "$install_dir"
	fi
	exit $ecode
}
trap cleanup EXIT

# Git Clone Spack
git clone "https://gitlab.com/nmsu_hpc/sstack.git" "$install_dir"

# Move To Install Directory
cd "$install_dir"

# Run Make
make build-prod
