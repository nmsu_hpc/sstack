#!/usr/bin/env bash

# Stop On Error
set -e

# Get Arguments
install_dir="$1"

# Setup ENV
source "${install_dir}/share/setup-env.sh" &>/dev/null

# Current Version
sstack --version
