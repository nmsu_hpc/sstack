#!/usr/bin/env bash

# Get Install Root
script_root="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# etc/spack
spack_dir="$(dirname "$script_root")"

# etc
spack_dir="$(dirname "$spack_dir")"

# Spack Install Root
spack_dir="$(dirname "$spack_dir")"

if command -v spack &>/dev/null; then
	# Deactivate Spack Environment
	if spack env status | grep -q 'In environment' &>/dev/null; then
		spack env deactivate
	fi

	# Unload All Modules
	if ! spack find --loaded | grep -q '0 loaded' &>/dev/null; then
		spack unload --all
	fi
fi

# Cleanup PATH
PATH="$(echo "$PATH" | tr ':' '\n' | grep -Exv "^\$|^${spack_dir}\$|^${spack_dir}/.*\$" | paste -sd ':' -)"
export PATH

# Cleanup MODULEPATH
MODULEPATH="$(echo "$MODULEPATH" | tr ':' '\n' | grep -Exv "^\$|^${spack_dir}\$|^${spack_dir}/.*\$" | paste -sd ':' -)"
export MODULEPATH

# Cleanup Env
# shellcheck disable=SC2046
{
	# Variables
	unset -v $(compgen -v | grep -Ei '^spack|_spack|^__spack')
	unset script_root
	unset spack_dir

	# Aliases
	if [ $(compgen -A 'alias' | grep -ic spack) -gt 0 ]; then
		unalias $(compgen -A 'alias' | grep -i spack)
	fi

	# Functions
	unset -f $(compgen -A 'function' | grep -Ei '^spack|^_spack|^__spack|_bash_completion_spack')
}
