help([[For help contact your site administrators.]])
whatis([[Name: SPACK]])
whatis([[Version: 0.17]])
whatis([[Target: x86_64]])
whatis([[Short description: A flexible package manager supporting multiple versions, configurations, platforms, and compilers.]])

family("spack")
conflict("conda", "micromamba", "easybuild")

local base = ""
local mdir = {}
local profd = pathJoin(base, "etc/spack/profile.d")

-- sh,dash,bash,zsh,csh,tcsh,ksh
local myShell = myShellName()

setenv("SPACK_ROOT",base)

-- disable user config scope
setenv("SPACK_USER_CONFIG_DIR", '/dev/null')

-- set modulepath
for i, item in pairs(mdir) do
    prepend_path("MODULEPATH", item)
end

-- set modulepath
prepend_path("MODULEPATH", base .. '/etc/spack/env-modules')

-- Source Environment Setup Scripts
if (myShell == "bash") then
    -- Load/Unload Commands
    if (mode() == "load") then
        local cmd = 'source ' .. pathJoin(profd,'setup-env.sh')
        execute{cmd=cmd, modeA={"load"}}
    elseif (mode() == "unload") then
        local cmd = 'source ' .. pathJoin(profd,'unsetup-env.sh')
        execute{cmd=cmd, modeA={"unload"}}
    end
end
