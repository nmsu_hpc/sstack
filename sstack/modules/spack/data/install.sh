#!/usr/bin/env bash

# Stop On Error
set -e

# Get Absolute Path To This Script
iscript_dir="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# Get Arguments
install_dir="$1"
version="$2"

# Set Trap to cleanup on exit
function cleanup() {
	ecode=$?
	if [ $ecode -ne 0 ]; then
		rm -rf "$install_dir"
	fi
	exit $ecode
}
trap cleanup EXIT

# Generate Branch Name
if [ "$version" == "develop" ]; then
	branch="develop"
else
	branch="releases/${version}"
fi

# Git Clone Spack
git clone -b "$branch" --depth 1 -c feature.manyFiles=true "https://github.com/spack/spack.git" "$install_dir"

# Copy Environment Scripts
mkdir "${install_dir}/etc/spack/profile.d"
cp -f "${iscript_dir}/setup-env.sh" "${install_dir}/etc/spack/profile.d/setup-env.sh"
cp -f "${iscript_dir}/unsetup-env.sh" "${install_dir}/etc/spack/profile.d/unsetup-env.sh"

# Copy Version Specific Config Files
cp -f "${iscript_dir}/${version}"/*.yaml "${install_dir}/etc/spack/"

# Setup Current Env
source "${install_dir}/etc/spack/profile.d/setup-env.sh"
export SPACK_USER_CONFIG_DIR=/dev/null

# Generate Compiler Config
spack compiler find --scope site

# Add Compiler to LMOD Core Compilers
## Get Compiler Specs
spec_list="$(spack config get compilers | grep 'spec' | sed -r 's/(spec:|[[:space:]])//g' | paste -s -d, - | sed -r "s/[^,]+/'&'/g" || true)"
if [ -n "$spec_list" ]; then
	spack config --scope site add "modules:default:lmod:core_compilers:[${spec_list}]"
fi

# Install Package To Generate Module Path
spack install zlib
