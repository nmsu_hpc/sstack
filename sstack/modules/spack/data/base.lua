help([[For help contact hpc-team@nmsu.edu]])
whatis([[Name : Spack Env]])
whatis([[Version: Latest]])
whatis([[Target: x86_64]])
whatis([[Short description :  This is a template to help create user-defined module files (env_name.lua) for Spack's environments.]])

family("spack_env")

-- Modify String In Quotes To Match Desired Environment Name or Path
local env_name = "base"
-- Don't forget to update line 2/3 aswell to update the Module Name/Version

execute{cmd="spack env activate -p " .. env_name,modeA={"load"}}
execute{cmd="spack env deactivate",modeA={"unload"}}
