#!/usr/bin/env bash

# Get Script Root
script_root="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# etc/spack
spack_dir="$(dirname "$script_root")"

# etc
spack_dir="$(dirname "$spack_dir")"

# Spack Install Root
spack_dir="$(dirname "$spack_dir")"

# Source Setup Script
source "${spack_dir}/share/spack/setup-env.sh"

# Cleanup MODULEPATH (setup script is to dumb to not load tcl module path when using lmod)
cleanup_path="${spack_dir}/share/spack/modules"
MODULEPATH="$(echo "$MODULEPATH" | tr ':' '\n' | grep -Exv "^\$|^${cleanup_path}/.*\$" | paste -sd ':' -)"
export MODULEPATH

# Unset Variables
unset script_root
unset spack_dir
unset cleanup_path
