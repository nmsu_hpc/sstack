#!/usr/bin/env bash

# Stop On Error
set -e

# Get Absolute Path To This Script
script_root="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# Get Arguments
install_dir="$1"
version="$2"

# src and prefix dirs
src_dir="$install_dir/src"
in_dir="$install_dir/prefix"


# Set Trap to cleanup on exit
function cleanup(){
	ecode=$?
	if [ $ecode -ne 0 ]; then
		rm -rf "$install_dir"
	fi
	exit $ecode
}
trap cleanup EXIT

# Create basic directories
mkdir -p "$install_dir"
mkdir -p "$src_dir"
mkdir -p "$in_dir"
mkdir -p "$install_dir/bin"

# To install pkgsrc
bmake="$in_dir/bin/bmake"

pkgsrc_ver="pkgsrc-${version}"
echo "$pkgsrc_ver"
# Checkout pkgsrc
cd "$src_dir"
cvs -danoncvs@anoncvs.NetBSD.org:/cvsroot checkout -r "$pkgsrc_ver" -P pkgsrc

# Bootstrap pkgsrc
"${src_dir}/pkgsrc/bootstrap/bootstrap" --prefix "$in_dir" --workdir "$in_dir/bootstrap/work" --unprivileged --make-jobs 4

# Copy install and unstall wrappers and make them executable
cp -f "$script_root/bin"/* "$install_dir/bin/"
chmod +x "$install_dir/bin"/*

# Install tools (We may need it later on)
# "$install_dir/bin/pkg-install" pkgin
# "$install_dir/bin/pkg-install" lintpkgsrc
