#!/usr/bin/env bash

# Stop On Error
set -e

# Get Arguments
install_dir="$1"

# Current Version
version=$(awk -F'/' 'NR==1 {print $6}' $install_dir/src/pkgsrc/CVS/Entries | awk -F'-' '{printf $2}')
# version=$(awk -F'-' '{printf $2}' $branch_name)

echo "$version"
