#!/usr/bin/env bash

# Stop On Error
set -e

# Get Arguments
install_dir="$1"

# To update pkgsrc via CVS, change to the pkgsrc directory and run cvs update
cd "$install_dir/src/pkgsrc"
cvs update -dP
