help([[For help contact your site administrators.]])
whatis([[Name: pkgsrc]])
whatis([[Version: Latest]])
whatis([[Target: x86_64]])
whatis([[Short description: pkgsrc is a framework for managing third-party software on UNIX-like systems.]])

family("pkgsrc")

local base = ""

-- sh,dash,bash,zsh,csh,tcsh,ksh
local myShell = myShellName()

prepend_path("MODULEPATH",pathJoin(base,'modules'))
setenv("PKGSRC_ROOT",base)
setenv("PKGSRCDIR",base)
setenv("PREFIX",  pathJoin(base,'prefix'))


local bmake = pathJoin(base, "prefix/bin")
prepend_path ("PATH", pathJoin(base, "prefix/bin"))
prepend_path ("PATH", pathJoin(base, "prefix/sbin"))
prepend_path ("LD_LIBRARY_PATH", pathJoin(base, "prefix/lib"))
prepend_path ("PATH", pathJoin(base, "bin"))
prepend_path ("C_INCLUDE_PATH", "/usr/bin")
prepend_path ("CPLUS_INCLUDE_PATH", "/usr/bin")
setenv("LOCALBASE", pathJoin(base, "prefix"))
