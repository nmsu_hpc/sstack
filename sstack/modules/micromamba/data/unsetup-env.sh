#!/usr/bin/env bash

# Get Install Root
script_root="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# Deactivate Mamba Env
if command -v micromamba &>/dev/null; then
	# shellcheck disable=SC2034
	for i in $(printenv | grep '^CONDA_PREFIX_'); do
		micromamba deactivate
	done
	micromamba deactivate
fi
# Cleanup PATH
PATH="$(echo "$PATH" | tr ':' '\n' | grep -Exv "^\$|^${script_root}\$|^${script_root}/.*\$" | paste -sd ':' -)"
export PATH

# Cleanup Env
# shellcheck disable=SC2046
{
	# Variables
	unset -v $(compgen -v | grep -Ei '^micromamba|_micromamba|^__micromamba')
	unset -v $(compgen -v | grep -Ei '^mamba|_mamba|^__mamba')
	unset -v $(compgen -v | grep -Ei '^conda|_conda|^__conda')

	# Functions
	unset -f $(compgen -A 'function' | grep -Ei '^micromamba|^_micromamba|^__micromamba')
	unset -f $(compgen -A 'function' | grep -Ei '^mamba|^_mamba|^__mamba')
	unset -f $(compgen -A 'function' | grep -Ei '^conda|^_conda|^__conda')
}

# unset Install root
unset script_root
