#!/usr/bin/env bash

# Stop On Error
set -e

# Get Absolute Path To This Script
script_root="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# Get Arguments
install_dir="$1"

# Setup Temp Directory
TMP_DIR="$(mktemp -d)"

# Set Trap to cleanup on exit
function cleanup(){
	ecode=$?
	if [ $ecode -ne 0 ]; then
		rm -rf "$install_dir"
	fi
	rm -rf "$TMP_DIR"
	exit $ecode
}
trap cleanup EXIT

# Download Latest Binary
curl -Ls "https://micromamba.snakepit.net/api/micromamba/linux-64/latest" | tar -xvj --strip-components=1 -C "$TMP_DIR" "bin/micromamba"
chmod +x "$TMP_DIR/micromamba"

# Install Base/Root Environment
export MAMBA_ROOT_PREFIX="$install_dir"
export MAMBA_EXE="${TMP_DIR}/micromamba"
"$MAMBA_EXE" create -y -f "${script_root}/base.yaml"

# Copy Config File
cp "${script_root}/condarc.yaml" "${install_dir}/.condarc"

# Copy Env Setup Scripts
cp "${script_root}/setup-env.sh" "${install_dir}/setup-env.sh"

# Copy Env Teardown Scripts
cp "${script_root}/unsetup-env.sh" "${install_dir}/unsetup-env.sh"

# Create Modules Directory
mkdir -p "${install_dir}/modules"
cp "${script_root}/base.lua" "${install_dir}/modules/base.lua"

# Cleanup
rm -rf "$TMP_DIR"
