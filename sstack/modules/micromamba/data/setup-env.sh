#!/usr/bin/env bash

#!/usr/bin/env bash

# Get Script Root
script_root="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# Setup Conda (if it exists)
if [ -f "${script_root}/etc/profile.d/conda.sh" ]; then
	source "${script_root}/etc/profile.d/conda.sh"
fi

# Setup Mamba (if it exists)
if [ -f "${script_root}/etc/profile.d/mamba.sh" ]; then
	source "${script_root}/etc/profile.d/mamba.sh"
	export MAMBA_NO_BANNER=1
fi

# Setup Micromamba (if it exists)
if [ -f "${script_root}/bin/micromamba" ]; then
	MAMBA_ROOT_PREFIX="$script_root"
	export MAMBA_ROOT_PREFIX

	MAMBA_EXE="$MAMBA_ROOT_PREFIX/bin/micromamba"
	export MAMBA_EXE

	# Setup Environment
	eval "$("$MAMBA_EXE" shell hook --shell bash --prefix "$MAMBA_ROOT_PREFIX")"
fi

# Activate Base Environment
micromamba activate base

# Unset Script Root
unset script_root
