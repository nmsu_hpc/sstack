help([[For help contact hpc-team@nmsu.edu]])
whatis([[Name : Micromamba Base Env]])
whatis([[Version: Latest]])
whatis([[Target: x86_64]])
whatis([[Short description :  This is a template to help create user-defined module files (env_name.lua) for micromamba's virtual environments.]])

family("mm_env")

-- Modify String In Quotes To Match Desired Environment Name or Path
local env_name = "base"
-- Don't forget to update line 2/3 aswell to update the Module Name/Version

execute{cmd="micromamba activate " .. env_name,modeA={"load"}}
execute{cmd="micromamba deactivate",modeA={"unload"}}
