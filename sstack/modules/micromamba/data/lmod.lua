help([[For help contact your site administrators.]])
whatis([[Name: MicroMamba (Anaconda)]])
whatis([[Version: Latest]])
whatis([[Target: x86_64]])
whatis([[Short description: Micromamba is a standalone version of Mamba which is an alternative to Conda. This stack delivers a custom anaconda/miniconda type deployment based on the communities 'conda-forge' channel with 'micromamba' as the package manager. ]])

family("micromamba")
conflict("conda", "spack", "easybuild")

local base = ""

-- sh,dash,bash,zsh,csh,tcsh,ksh
local myShell = myShellName()

setenv("MICROMAMBA_ROOT",base)
prepend_path("MODULEPATH",pathJoin(base,'modules'))

-- Source Environment Setup Scripts
if (myShell == "bash") then
    -- Load/Unload Commands
    if (mode() == "load") then
        local cmd = 'source ' .. pathJoin(base,'setup-env.sh')
        execute{cmd=cmd, modeA={"load"}}
    elseif (mode() == "unload") then
        local cmd = 'source ' .. pathJoin(base,'unsetup-env.sh')
        execute{cmd=cmd, modeA={"unload"}}
    end
end
