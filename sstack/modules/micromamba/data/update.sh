#!/usr/bin/env bash

# Stop On Error
set -e

# Get Arguments
mamba_root="$1"

# Setup ENV
source "$mamba_root/setup-env.sh"

# Setup Base/Root Environment
micromamba activate base
micromamba update -y python
micromamba update -y --all
