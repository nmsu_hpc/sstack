import os
import subprocess
from sstack.modules.helpers import *

mod_root = os.path.dirname(__file__)

def install(stack: sstack_stack):

    # Set Data Config File Root
    data_root = os.path.join(mod_root,"data")

    # Install Stack
    if os.path.exists(stack.path):
        print("Failed: instance already exists.")
        exit(1)
    else:
        # Run Installation
        script = os.path.join(data_root, 'install.sh')
        os.system('bash "{}" "{}"'.format(script, stack.path))


def list_sub_directories(folder_path: str):
    # Get the sub_folders' name in folder_path
    sub_names = []
    for item in os.listdir(folder_path):
        item_path = os.path.join(folder_path, item)
        if os.path.isdir(item_path) and not item.startswith('.'):
            sub_names.append(item)
    return sub_names


def list_lua_files(folder_path: str):
    # Get the lua files' name in folder_path
    lua_files = []
    for item in os.listdir(folder_path):
        item_path = os.path.join(folder_path, item)
        if os.path.isfile(item_path) and item.endswith('.lua') and not item.startswith('.'):
            lua_files.append(item)
    return lua_files


def prompt_user_for_deletion(module_name: str):
    while True:
        user_input = input(f"Module '{module_name}' is not in your environment installation folder. Do you want to delete it? (y/n): ").strip().lower()
        if user_input in ['y', 'n']:
            return user_input == 'y'
        else:
            print("Invalid input. Please enter 'y' or 'n'.")


def delete_module_file(module_name: str, folder_path: str):
    file_path = os.path.join(folder_path, f"{module_name}.lua")
    if os.path.exists(file_path):
        os.remove(file_path)
        print(f"Deleted {file_path}")
    else:
        print(f"File {file_path} does not exist.")


def generate_module(stack: sstack_stack):

    # Set Data Config File Root
    data_root = os.path.join(mod_root,"data")

    # Ensure Module Directory Exists
    parent_dir = os.path.dirname(stack.module)
    if not os.path.exists(parent_dir):
        os.mkdir(parent_dir)

    # Open Source and Destination Files
    src = open(os.path.join(data_root,"lmod.lua"), "r")
    destFile = open(stack.module, "w")

    # Write source to destination and replace lines as needed
    for line in src:
        if "local base" in line:
            destFile.write("local base = \"{0}\"\n".format(stack.path))
        elif "Version" in line:
            destFile.write("whatis([[Version: {}]])\n".format(stack.version))
        else:
            destFile.write(line)

    # Close Open Files
    src.close()
    destFile.close()

    """Create .lua files for user defined envs"""
    # Get ENVs and Destination Folders
    envsfolder = os.path.join(stack.path,"envs")
    destfolder = os.path.join(stack.path,"modules")

    # Do it when envsfolder exists
    if os.path.exists(envsfolder):
        # Read Base File
        base_lua_path  = os.path.join(data_root,"base.lua")
        with open(base_lua_path, 'r') as base_file:
            base_content = base_file.read()

        # Get ENVs list
        mamba_envs = list_sub_directories(envsfolder)

        # Make sure destfolder exist
        os.makedirs(destfolder, exist_ok=True)

        # Create .lua files based on mamba_envs
        for env in mamba_envs:
            lua_file_path = os.path.join(destfolder, f'{env}.lua')
            # Check if env.lua already exists. If so, skip it.
            if os.path.exists(lua_file_path):
                continue

            # Replace line 2 and line 10 in base_content
            modified_content = base_content.replace(
                'whatis([[Name : Micromamba Base Env]])',
                f'whatis([[Name : {env}]])'
            ).replace(
                'local env_name = "base"',
                f'local env_name = "{env}"'
            )

            # Write into env.lua
            with open(lua_file_path, 'w') as lua_file:
                lua_file.write(modified_content)

    # Search the module folder, ask user if the module need to be deleted (if it's not in mamba_envs list)
    if os.path.exists(destfolder):
        module_list = list_lua_files(destfolder)
        if 'base.lua' in module_list:  # ignore 'base.lua'
            module_list.remove('base.lua')

        env_list = []
        if os.path.exists(envsfolder):
            env_list = list_sub_directories(envsfolder)

        for file_name in module_list:
            module_name = file_name.rstrip(".lua")
            if module_name not in env_list:  # module exists, but the corresponding ENV has been removed.
                if prompt_user_for_deletion(module_name):  # prompt user. if user input y, then True; if n, then False.
                    delete_module_file(module_name, destfolder)
                else:
                    print(f"Skipped deleting {module_name}")


def update(stack: sstack_stack) -> None:

    # Set Data Config File Root
    data_root = os.path.join(mod_root,"data")

    # Run Update Script
    script = os.path.join(data_root, 'update.sh')
    os.system('bash "{}" "{}"'.format(script, stack.path))

def get_version(stack: sstack_stack) -> str:

    # Set Data Config File Root
    data_root = os.path.join(mod_root,"data")

    # Run Get Version Script
    script = os.path.join(data_root, 'getver.sh')
    cmd = 'bash "{}" "{}"'.format(script, stack.path)
    version = subprocess.check_output(cmd, shell=True, universal_newlines=True).strip()

    return version
