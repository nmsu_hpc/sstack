#!/usr/bin/env bash

# Stop On Error
set -e

# Get Absolute Path To This Script
script_root="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# Get Arguments
install_dir="$1"

# Setup Temp Directory
TMP_DIR="$(mktemp -d)"

# Set Trap to cleanup on exit
function cleanup(){
	ecode=$?
	if [ $ecode -ne 0 ]; then
		rm -rf "$install_dir"
	fi
	rm -rf "$TMP_DIR"
	exit $ecode
}
trap cleanup EXIT

# Create basic directories
mkdir -p "$install_dir/modules"
mkdir -p "$install_dir/containers"
mkdir -p "$install_dir/registry"

# Download Latest Binary
curl -Ls "https://micromamba.snakepit.net/api/micromamba/linux-64/latest" | tar -xvj --strip-components=1 -C "$TMP_DIR" "bin/micromamba"
chmod +x "$TMP_DIR/micromamba"

# Install Base/Root Environment
export MAMBA_ROOT_PREFIX="$install_dir/mmenv"
export MAMBA_EXE="${TMP_DIR}/micromamba"
"$MAMBA_EXE" create -y -f "${script_root}/base.yaml"

# Copy Config File
cp "${script_root}/condarc.yaml" "${install_dir}/mmenv/.condarc"

# Copy Env Setup Scripts
cp "${script_root}/setup-env.sh" "${install_dir}/mmenv/setup-env.sh"

# Copy Env Teardown Scripts
cp "${script_root}/unsetup-env.sh" "${install_dir}/mmenv/unsetup-env.sh"

# Setup SHPC
source "${install_dir}/mmenv/setup-env.sh"
shpc config set "module_base:$install_dir/modules"
shpc config set "container_base:$install_dir/containers"
shpc config add "registry:$install_dir/registry"
