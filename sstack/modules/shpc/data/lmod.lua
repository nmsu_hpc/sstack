help([[For help contact your site administrators.]])
whatis([[Name: Singularity-hpc]])
whatis([[Version: Latest]])
whatis([[Target: x86_64]])
whatis([[Short description: Singularity Registry HPC (shpc) allows you to install containers as modules.]])

family("shpc")

local base = ""

-- sh,dash,bash,zsh,csh,tcsh,ksh
local myShell = myShellName()

prepend_path("MODULEPATH",pathJoin(base,'modules'))
setenv("SHPC_ROOT",base)

-- Source Environment Setup Scripts
if (myShell == "bash") then
    -- Load/Unload Commands
    if (mode() == "load") then
        local cmd = 'source ' .. pathJoin(base,'mmenv/setup-env.sh')
        execute{cmd=cmd, modeA={"load"}}
    elseif (mode() == "unload") then
        local cmd = 'source ' .. pathJoin(base,'mmenv/unsetup-env.sh')
        execute{cmd=cmd, modeA={"unload"}}
    end
end
