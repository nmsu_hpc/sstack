#!/usr/bin/env bash

# Stop On Error
set -e

# Get Arguments
install_dir="$1"

# Setup ENV
source "$install_dir/mmenv/setup-env.sh" &>/dev/null

# Current Version
shpc --version
