#!/usr/bin/env bash

# Stop On Error
set -e

# Get Arguments
install_dir="$1"

# Setup ENV
source "$install_dir/mmenv/setup-env.sh"

# Setup Base/Root Environment
micromamba activate base
micromamba update -y python
micromamba update -y --all

# Reapply SHPC Settings
shpc config set "module_base:$install_dir/modules"
shpc config set "container_base:$install_dir/containers"
shpc config add "registry:$install_dir/registry"
