import dataclasses
import dataclasses_json
import functools
import json
import typing
import os
import pathlib
import sys
import prettytable

#
# Classes
#
@functools.total_ordering
@dataclasses_json.dataclass_json
@dataclasses.dataclass(frozen=True, eq=False)
class sstack_stype:
    name: str
    versions: typing.List[str]
    default: str
    description: str

    def __eq__(self, other) -> bool:
        return self.name == other.name

    def __lt__(self, other) -> bool:
        return self.name < other.name

    def val_ver(self, ver: str) -> bool:
        if ver in self.versions:
            return True
        else:
            return False

@dataclasses_json.dataclass_json
@dataclasses.dataclass(frozen=True)
class sstack_sconfig:
    default: str
    types: typing.List[sstack_stype]

    def val_type(self, name: str) -> bool:
        for item in self.types:
            if item.name == name:
                return True
        return False

    def get_stype(self, name: str) -> sstack_stype:
        if self.val_type(name):
            for item in self.types:
                if item.name == name:
                    return item
            return None
        else:
            return None

    def vis_types(self) -> None:
        t = prettytable.PrettyTable(['Type (Name)','Versions', 'Description'])
        t.hrules = prettytable.ALL
        t.align = "l"
        t.valign = "m"
        t.max_width = 30
        for item in self.types:
            t.add_row([item.name, item.versions, item.description])
        print(t)

@functools.total_ordering
@dataclasses_json.dataclass_json
@dataclasses.dataclass(frozen=True, eq=False)
class sstack_stack:
    name: str
    type: str
    version: str
    path: str
    module: str

    def __eq__(self, other) -> bool:
        return (self.name == other.name and self.type == other.type)

    def __lt__(self, other) -> bool:
        return (self.type, self.name) < \
            (other.type, other.name)

    def val_path(self) -> bool:
        val_path = pathlib.Path(self.path)
        return val_path.is_dir()

    def val_module(self) -> bool:
        val_path = pathlib.Path(self.module)
        return val_path.is_file()

    def vis(self) -> None:
        t = prettytable.PrettyTable(['Name','Type', 'Version', 'Path'])
        t.hrules = prettytable.ALL
        t.align = "l"
        t.valign = "m"
        t.max_width = 40
        t.add_row([self.name, self.type, self.version, self.path])
        print(t)

@dataclasses_json.dataclass_json
@dataclasses.dataclass
class sstack_droot:
    root: str
    jfile: str = None
    sdir: str = None
    mdir: str = None
    stacks: typing.List[sstack_stack] = dataclasses.field(default_factory=list)

    def __post_init__(self) -> None:
        if self.jfile == None:
            self.jfile = os.path.join(self.root, "stacks.json")
        if self.sdir == None:
            self.sdir = os.path.join(self.root, "stacks")
        if self.mdir == None:
            self.mdir = os.path.join(self.root, "modules")

    def write(self) -> None:
        self.stacks.sort()
        with open(self.jfile, "w") as jfile_new:
            jfile_new.write(self.to_json())

    def ensure_tree(self) -> None:
        if not os.path.exists(self.root):
            os.makedirs(self.root)
        if not os.path.exists(self.sdir):
            os.mkdir(self.sdir)
        if not os.path.exists(self.mdir):
            os.mkdir(self.mdir)

    def val_stack(self, name: str, type: str) -> bool:
        for item in self.stacks:
            if item.name == name and item.type == type:
                return True
        return False

    def get_stack(self, name: str, type: str) -> sstack_stack:
        if self.val_stack(name, type):
            for item in self.stacks:
                if item.name == name and item.type == type:
                    return item
            return None
        else:
            return None

    def vis_stacks(self) -> None:
        t = prettytable.PrettyTable(['Name','Type', 'Version', 'Path'])
        t.hrules = prettytable.ALL
        t.align = "l"
        t.valign = "m"
        t.max_width = 60
        for item in self.stacks:
            t.add_row([item.name, item.type, item.version, item.path])
        print(t)
        print()

    def vis_modules(self) -> None:
        print('Module Tree ({}):'.format(self.mdir))
        print()
        ptree(self.mdir)
        print()
        print('Notes:')
        print('- "default ->" represent default module overrides using symlinks.')
        print('  - Destination becomes default module for that stack type.')
        print('  - To modify override see "sstack modules default --help".')

#
# Functions
#
def json_dict_from_file(path: str) -> dict:
    with open(path, 'r') as f:
        return json.load(f)

def ensure_droot(path: str) -> sstack_droot:
    jfile = os.path.join(path, "stacks.json")
    if os.path.exists(jfile):
        sstack_data: sstack_droot = sstack_droot.from_dict(json_dict_from_file(jfile))
        sstack_data.ensure_tree()
    else:
        sstack_data: sstack_droot = sstack_droot(path)
        sstack_data.ensure_tree()
        sstack_data.write()
    return sstack_data

def query_yes_no(question, default="no") -> bool:
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
            It must be "yes" (the default), "no" or None (meaning
            an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True, "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == "":
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' " "(or 'y' or 'n').\n")
# Thanks to https://acrisel.github.io/posts/2017/08/python-print-directory-tree/
def realname(path, root=None):
    if root is not None:
        path=os.path.join(root, path)
    result=os.path.basename(path)
    if os.path.islink(path):
        realpath=os.readlink(path)
        result= '%s -> %s' % (os.path.basename(path), realpath)
    return result

# Thanks to https://acrisel.github.io/posts/2017/08/python-print-directory-tree/
def ptree(startpath, depth=-1):
    prefix=0
    if startpath != '/':
        if startpath.endswith('/'): startpath=startpath[:-1]
        prefix=len(startpath)
    for root, dirs, files in os.walk(startpath):
        level = root[prefix:].count(os.sep)
        if depth >-1 and level > depth: continue
        indent=subindent =''
        if level > 0:
            indent = '|   ' * (level-1) + '|-- '
        subindent = '|   ' * (level) + '|-- '
        print('{}{}/'.format(indent, realname(root)))
        # print dir only if symbolic link; otherwise, will be printed as root
        for d in dirs:
            if os.path.islink(os.path.join(root, d)):
                print('{}{}'.format(subindent, realname(d, root=root)))
        for f in files:
            print('{}{}'.format(subindent, realname(f, root=root)))

def is_subpath(path, parent_path):
    """Check if a path is a subpath of another path."""

    # Convert to absolute paths for reliable comparison
    path = os.path.abspath(path)
    parent_path = os.path.abspath(parent_path)

    # Use the commonpath method from os.path
    return os.path.commonpath([path, parent_path]) == parent_path
