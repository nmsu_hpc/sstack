#!/usr/bin/env bash

# Stop On Error
set -e

# Get Arguments
install_dir="$1"

# Setup ENV
source "$install_dir/setup-env.sh"
export MAMBA_NO_BANNER=1

# Setup Base/Root Environment
mamba activate base
mamba update -y python
mamba update -y --all
