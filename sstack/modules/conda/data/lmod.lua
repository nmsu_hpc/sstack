help([[For help contact your site administrators.]])
whatis([[Name: Conda]])
whatis([[Version: Latest]])
whatis([[Target: x86_64]])
whatis([[Short description: Conda is an open source package management and environment management system. This stack delivers a custom anaconda/miniconda type deployment based on the communities 'conda-forge' channel with 'conda'/'mamba' as the package manager.]])

family("conda")
conflict("micromamba","spack","easybuild")

local base = ""

-- sh,dash,bash,zsh,csh,tcsh,ksh
local myShell = myShellName()

setenv("CONDA_ROOT", base)
prepend_path("PATH",pathJoin(base,"condabin"))
prepend_path("MODULEPATH",pathJoin(base,'modules'))

-- Source Environment Setup Scripts
if (myShell == "bash") then
    -- Load/Unload Commands
    if (mode() == "load") then
        local cmd = 'source ' .. pathJoin(base,'setup-env.sh')
        execute{cmd=cmd, modeA={"load"}}
    elseif (mode() == "unload") then
        local cmd = 'source ' .. pathJoin(base,'unsetup-env.sh')
        execute{cmd=cmd, modeA={"unload"}}
    end
end
