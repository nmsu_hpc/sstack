help([[For help contact hpc-team@nmsu.edu]])
whatis([[Name : Conda Base Env]])
whatis([[Version: Latest]])
whatis([[Target: x86_64]])
whatis([[Short description :  This is a template to help create user-defined module files (env_name.lua) for Conda's virtual environments.]])

family("conda_env")

-- Modify String In Quotes To Match Desired Environment Name or Path
local env_name = "base"
-- Don't forget to update line 2/3 aswell to update the Module Name/Version

execute{cmd="conda activate " .. env_name,modeA={"load"}}
execute{cmd="conda deactivate",modeA={"unload"}}
