#!/usr/bin/env bash

# Stop On Error
set -e

# Get Arguments
install_dir="$1"

# Setup ENV
source "$install_dir/setup-env.sh" &>/dev/null

# Current Version
version="$(conda --version | sed 's/conda//g' | tr -d '[:space:]')"
echo "$version"
