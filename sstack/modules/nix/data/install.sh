#!/usr/bin/env bash

# Stop On Error
set -e

# Get Absolute Path To This Script
script_root="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# Get Arguments
install_dir="$1"

# Setup Temp Directory
TMP_DIR="$(mktemp -d)"
chmod 1777 "$TMP_DIR"

# Set Trap to cleanup on exit
function cleanup(){
	ecode=$?
	if [ $ecode -ne 0 ]; then
		chmod -R u+w "$install_dir"
		rm -rf "$install_dir"
	fi
	rm -rf "$TMP_DIR"
	exit $ecode
}
trap cleanup EXIT

# Create Install Directories and Nix Bind Files
mkdir -p "$install_dir"
mkdir -p "$install_dir/bin"
mkdir -p "$install_dir/nix"
mkdir -p "$install_dir/home/.cache/nix"
mkdir -p "$install_dir/home/.config/nix"
mkdir -p "$install_dir/home/.config/nixpkgs"
mkdir -p "$install_dir/home/.local/state/nix"
mkdir -p "$install_dir/home/.nix-defexpr"
touch "$install_dir/home/.nix-channels"

# Copy Bin Files
cp -f "$script_root/bin"/* "$install_dir/bin/"

# Build Wrapper Container
apptainer build -F --bind="$TMP_DIR:/tmp" "$install_dir/bin/nix.sif" "$script_root/nix.def"

# Install Nix
"$install_dir/bin/sif_launch.sh" bash -c 'curl -L https://nixos.org/nix/install | sh'

# Remove bash profile changes
sed -i "/# added by Nix installer/d" "$HOME/.bash_profile"
