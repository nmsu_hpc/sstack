help([[For help contact your site administrators.]])
whatis([[Name: Nix]])
whatis([[Version: Latest]])
whatis([[Target: x86_64]])
whatis([[Short description: Nix is a tool that takes a unique approach to package management and system configuration. Learn how to make reproducible, declarative and reliable systems.]])

family("nix")

local base = ""

setenv("NIX_ROOT", base)
prepend_path("PATH",base .. '/bin')
