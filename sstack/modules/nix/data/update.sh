#!/usr/bin/env bash

# Stop On Error
set -e

# Get Arguments
install_dir="$1"

# Get Absolute Path To This Script
script_root="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# Copy Bin Files
cp -f "$script_root/bin"/* "$install_dir/bin/"

# Setup Temp Directory
TMP_DIR="$(mktemp -d)"
chmod 1777 "$TMP_DIR"

# Get latest nix container
apptainer build -F --bind="$TMP_DIR:/tmp" "$install_dir/bin/nix.sif" "$script_root/nix.def"

# Cleanup Temp Directory
rm -rf "$TMP_DIR"

# Update Nix
"$install_dir/bin/nix-channel" --update
"$install_dir/bin/nix-env" --install --attr nixpkgs.nix nixpkgs.cacert
