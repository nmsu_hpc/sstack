#!/usr/bin/env bash

set -e

# Set Vars
script_root="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
install_dir="$(dirname "$script_root")"

# Validate SIF File
if [ -z "$SIF" ] || [ ! -f "$SIF" ]; then
	if [ -f "${script_root}/nix.sif" ]; then
		export SIF="${script_root}/nix.sif"
	else
		echo "'SIF' Environment Variable is not set and 'nix.sif' cannot be found!"
		echo "Stopping!"
		exit 2
	fi
	unset script_root
fi

# Validate XDG_RUNTIME_DIR
if [ -n "$XDG_RUNTIME_DIR" ] && [ -d "$XDG_RUNTIME_DIR" ]; then
	export APPTAINER_BIND="$XDG_RUNTIME_DIR"
else
	unset XDG_RUNTIME_DIR
fi

# Validate Needed Directories/Files in $HOME
if [ ! -d "$HOME/.cache/nix" ]; then
	mkdir -p "$HOME/.cache/nix"
fi
if [ ! -d "$HOME/.config/nix" ]; then
	mkdir -p "$HOME/.config/nix"
fi
if [ ! -d "$HOME/.config/nixpkgs" ]; then
	mkdir -p "$HOME/.config/nixpkgs"
fi
if [ ! -d "$HOME/.local/state/nix" ]; then
	mkdir -p "$HOME/.local/state/nix"
fi
if [ ! -d "$HOME/.nix-defexpr" ]; then
	mkdir -p "$HOME/.nix-defexpr"
fi


# Launch Command
bind="$install_dir/nix:/nix"
bind+=",$install_dir/home/.cache/nix:$HOME/.cache/nix"
bind+=",$install_dir/home/.config/nix:$HOME/.config/nix"
bind+=",$install_dir/home/.config/nix:$HOME/.config/nixpkgs"
bind+=",$install_dir/home/.local/state/nix:$HOME/.local/state/nix"
bind+=",$install_dir/home/.nix-defexpr:$HOME/.nix-defexpr"
bind+=",$install_dir/home/.nix-channels:$HOME/.nix-channels"
if [ -z "$CUDA_VISIBLE_DEVICES" ]; then
	apptainer --silent run --bind="$bind" "$SIF" "$@"
else
	apptainer --silent run --bind="$bind" --nv "$SIF" "$@"
fi
