#!/usr/bin/env bash

# Stop On Error
set -e

# Get Arguments
install_dir="$1"

# Current Version
version="$("$install_dir/bin/nix" --version | awk '{print $3}')"
echo "$version"
