help([[For help contact your site administrators.]])
whatis([[Name: EASYBUILD]])
whatis([[Version: Latest]])
whatis([[Target: x86_64]])
whatis([[Short description: EasyBuild is a software build and installation framework that allows you to manage (scientific) software on High Performance Computing (HPC) systems in an efficient way.]])

family("easybuild")
conflict("spack", "conda", "micromamba")

local base = ""

-- sh,dash,bash,zsh,csh,tcsh,ksh
local myShell = myShellName()

-- Set Easybuild Variables
setenv("EASYBUILD_CONFIGFILES", pathJoin(base, "config.cfg"))
setenv("EB_PYTHON", pathJoin(base, "venv/bin/python"))

-- Set up Path
prepend_path("PATH", pathJoin(base, "venv/bin"))

-- Set Python Variables
setenv("VIRTUAL_ENV", pathJoin(base, "venv"))
unsetenv("PYTHONHOME")

-- Set Modulepath for Software
prepend_path("MODULEPATH", pathJoin(base, "modules/all"))

-- Source Environment Setup Scripts
if (myShell == "bash") then
    -- Load/Unload Commands
    if (mode() == "load") then
        local vbin = pathJoin(base, "venv/bin")
        local cmd = string.format("source %s/minimal_bash_completion.bash; ", vbin) ..
            string.format("source %s/optcomplete.bash; ", vbin) ..
            string.format("source %s/eb_bash_completion.bash", vbin)
        execute{cmd=cmd, modeA={"load"}}
    elseif (mode() == "unload") then
        local cmd = "complete -r eb; " ..
             "unset -f _eb"
        execute{cmd=cmd, modeA={"unload"}}
    end
end
