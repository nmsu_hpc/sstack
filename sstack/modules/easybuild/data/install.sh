#!/usr/bin/env bash

# Stop On Error
set -e

# Get Absolute Path To This Script
iscript_dir="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# Get Arguments
install_dir="$1"

# Set Trap to cleanup on exit
function cleanup(){
	ecode=$?
	if [ $ecode -ne 0 ]; then
		rm -rf "$install_dir"
	fi
	exit $ecode
}
trap cleanup EXIT

# Create Install Dir
mkdir "$install_dir"

# Install Easybuild
python3 -m venv "$install_dir/venv"
source "$install_dir/venv/bin/activate"
python3 -m pip install --upgrade pip wheel
python3 -m pip install easybuild

# setup config file
cat <<-EOF > "${install_dir}/config.cfg"
	[config]
	prefix=${install_dir}
	modules-tool=Lmod
EOF

# Create Module Dir
mkdir -p "${install_dir}/modules/all"
