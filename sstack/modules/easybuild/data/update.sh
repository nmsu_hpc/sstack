#!/usr/bin/env bash

# Stop On Error
set -e

# Get Arguments
install_dir="$1"

# Activate Python Env
source "${install_dir}/venv/bin/activate"

# Upgrade Easybuild]
python3 -m pip install --upgrade easybuild
