#!/usr/bin/env python3
import click
import os
import shutil
import pathlib
import dataclasses
import distro
import re
from .modules.helpers import *

# Load Config File (Version + Stack Type Information)
script_root = os.path.dirname(os.path.abspath(__file__))
sconfig_jdata = json_dict_from_file(os.path.join(script_root, "config.json"))
sconfig = sstack_sconfig.from_dict(sconfig_jdata)

# Dynamically Import Stack Specific Python Functions
for item in sconfig.types:
    exec('from .modules import {} as {}'.format(item.name, item.name))

# Set common help text
help_path = "Data Root (Path of stacks/modules)"
help_type = "Stack Type"
help_name = "Stack Name"

# determine default data root (--path)
def_home = os.path.join(os.path.expanduser('~'), "sstack")
dis_name = '{}_{}'.format(distro.id(), distro.major_version())
dis_name = dis_name.lower().strip()
dis_name = re.sub(r"-", ' ', dis_name)
dis_name = re.sub(r"[^\w\s]", '', dis_name)
dis_name = re.sub(r"\s+", '_', dis_name)
def_home = os.path.join(def_home, dis_name)

# Create main click groups
@click.group()
@click.version_option(message="%(version)s")
def cli():
    pass

@cli.group(help='List installed stacks, available types, etc..')
def show():
    pass

@cli.group(help='Manage installed module files.')
def modules():
    pass

@modules.group('default', help='Manage default module for each stack type.')
def modules_default():
    pass

# sstack show types
@show.command(help='Show available stack types.')
def types():
    sconfig.vis_types()

# sstack show stacks
@show.command('stacks', help='Show installed software stacks.')
@click.option('--path', '-p',
    type=click.Path(
        exists=True,
        file_okay=False,
        dir_okay=True,
        writable=False,
        readable=True,
        resolve_path=True,
        allow_dash=False,
        path_type=None,
    ),
    default=def_home,
    envvar='SSTACK_PATH',
    show_default=True,
    help=help_path,
)
def list(path: str):
    droot = ensure_droot(path)
    droot.vis_stacks()
    print()

# sstack show modules
@show.command('modules', help='Show stack modules.')
@click.option('--path', '-p',
    type=click.Path(
        exists=True,
        file_okay=False,
        dir_okay=True,
        writable=False,
        readable=True,
        resolve_path=True,
        allow_dash=False,
        path_type=None,
    ),
    default=def_home,
    envvar='SSTACK_PATH',
    show_default=True,
    help=help_path,
)
def showmod(path: str):
    droot = ensure_droot(path)
    droot.vis_modules()
    print()

# sstack install
@cli.command(help='Install new software stack.')
@click.option('--path', '-p',
    type=click.Path(
        exists=False,
        file_okay=False,
        dir_okay=True,
        writable=True,
        readable=True,
        resolve_path=True,
        allow_dash=False,
        path_type=None,
    ),
    default=def_home,
    envvar='SSTACK_PATH',
    show_default=True,
    help=help_path,
)
@click.option('--type', '-t',
    required=True,
    envvar='SSTACK_TYPE',
    help=help_type,
)
@click.option('--version', '-v',
    help='Version of stack type to install',
)
@click.option('--name', '-n',
    required=True,
    envvar='SSTACK_NAME',
    help=help_name,
)
@click.pass_context
def install(ctx, path: str, type: str, version: str, name: str):
    droot = ensure_droot(path)

    # Set and Validate Type
    if sconfig.val_type(type):
        stype: sstack_stype = sconfig.get_stype(type)
    else:
        print ("'" + type + "' is not a valid type! Stopping!!")
        exit(2)

    # Set and Validate version
    if version is None:
        version = stype.default
    else:
        if not stype.val_ver(version):
            print (version + " is not a valid version! Stopping!!")
            exit(3)

    # Ensure Stack Type Install Directory Exists
    dest_type = os.path.join(droot.sdir, type)
    if not os.path.exists(dest_type):
        os.mkdir(dest_type)

    # Set Install Destination and Validate
    dest = os.path.join(dest_type, name)
    if droot.val_stack(name, type):
        question="Stack Exists (name={}, type={})! Overwrite?".format(name, type)
        if query_yes_no(question, default="no"):
            ctx.invoke(remove, path=path, type=type, name=name)
            droot = ensure_droot(path)
        else:
            exit(1)
    elif os.path.exists(dest):
        question="Stack Exists but is not registered ({})! Overwrite?".format(dest)
        if query_yes_no(question, default="no"):
            shutil.rmtree(dest)
        else:
            exit(1)

    # Generate Module File Path
    tmdir = os.path.join(droot.mdir, stype.name)
    mfile = os.path.join(tmdir, name) + ".lua"

    # Create Initial Stack Variable
    stack = sstack_stack(name, stype.name, version, dest, mfile)

    # Install Stack
    globals()[stack.type].install(stack)

    # Create New Stack Variable With Updated Version
    new_ver = globals()[stack.type].get_version(stack)
    nstack: sstack_stack = dataclasses.replace(stack, version=new_ver)

    # Install ModuleFile
    globals()[nstack.type].generate_module(nstack)

    # Register New Stack
    droot.stacks.append(nstack)
    droot.write()

    # Print Success
    print()
    print()
    print('Stack Successfully Installed!')
    nstack.vis()
    print()
    print('Module Setup/Load Commands:')
    print()
    print('module use "{}"'.format(droot.mdir))
    print('module load {}/{}'.format(nstack.type, nstack.name))

    # If it is conda/micromamba/spack, let user know: to generate module for envs, run sstack update --du
    if type in ['conda', 'micromamba', 'spack']:
        print()
        print('To generate corresponding modules for {} ENVs you created:'.format(type))
        print()
        print('sstack update -t {} -n {} --du'.format(type, nstack.name))

    print()

# sstack remove
@cli.command(help='Remove existing software stack.')
@click.option('--path', '-p',
    type=click.Path(
        exists=True,
        file_okay=False,
        dir_okay=True,
        writable=True,
        readable=True,
        resolve_path=True,
        allow_dash=False,
        path_type=None,
    ),
    default=def_home,
    envvar='SSTACK_PATH',
    show_default=True,
    help=help_path,
)
@click.option('--type', '-t',
    required=True,
    envvar='SSTACK_TYPE',
    help=help_type,
)
@click.option('--name', '-n',
    required=True,
    envvar='SSTACK_NAME',
    help=help_name,
)
def remove(path: str, type: str, name: str):
    droot = ensure_droot(path)

    # Set Install Destination and Validate
    if droot.val_stack(name, type):
        dstack: sstack_stack = droot.get_stack(name, type)
    else:
        print("The stack '" + name + "' is not registered! Stopping!!")
        exit(4)

    # Fix Read Only Files
    if type == "nix":
        os.system('chmod -R u+w {}'.format(dstack.path))

    # Cleanup Install Directory
    if os.path.exists(dstack.path):
        shutil.rmtree(dstack.path)

    # Cleanup ModuleFile
    if os.path.exists(dstack.module):
        os.remove(dstack.module)

    # Unregister Stack
    droot.stacks.remove(dstack)
    droot.write()

    # Report Success
    print('Stack Successfully Removed!')
    print('Remaining Stacks:')
    droot.vis_stacks()

# sstack update
@cli.command(help='Update software stack and regenerate modulefile.')
@click.option('--path', '-p',
    type=click.Path(
        exists=True,
        file_okay=False,
        dir_okay=True,
        writable=True,
        readable=True,
        resolve_path=True,
        allow_dash=False,
        path_type=None,
    ),
    default=def_home,
    envvar='SSTACK_PATH',
    show_default=True,
    help=help_path,
)
@click.option('--type', '-t',
    required=True,
    envvar='SSTACK_TYPE',
    help=help_type,
)
@click.option('--name', '-n',
    required=True,
    envvar='SSTACK_NAME',
    help=help_name,
)
@click.option('--du', 'disup',
    required=False,
    is_flag=True,
    show_default=True,
    default=False,
    help="Don't Update Stack Software.",
)
@click.option('--dm', 'dismod',
    required=False,
    is_flag=True,
    show_default=True,
    default=False,
    help="Don't Regenerate ModuleFile.",
)
def update(path: str, type: str, name: str, disup: bool, dismod: bool):
    droot = ensure_droot(path)

    # Validate Stack Exists
    if droot.val_stack(name, type):
        stack: sstack_stack = droot.get_stack(name, type)
    else:
        print('Stack "{}", of type "{}", does not exist! Stopping'.format(name,type))
        exit(5)

    if not disup:
        # Update Stack
        globals()[stack.type].update(stack)
        print()
        print('{}/{}: Sucessfully Updated!'.format(type,name))

        # Generate Updated Stack Variable
        new_ver = globals()[stack.type].get_version(stack)
        nstack: sstack_stack = dataclasses.replace(stack, version=new_ver)

        # Update Registration
        droot.stacks.remove(stack)
        droot.stacks.append(nstack)
        droot.write()

        # Update stack variable
        stack: sstack_stack = droot.get_stack(name, type)

    # Update Module File
    if not dismod:
        globals()[stack.type].generate_module(stack)
        print()
        print('{}/{}: Sucessfully Regenerated Module File!'.format(type,name))

    # Report Success
    print()
    stack.vis()
    print()
    print('Module Setup/Load Commands:')
    print()
    print('module use "{}"'.format(droot.mdir))
    print('module load {}/{}'.format(stack.type, stack.name))
    print()

# sstack modules show
@modules.command('show', help='Show stack modules.')
@click.option('--path', '-p',
    type=click.Path(
        exists=True,
        file_okay=False,
        dir_okay=True,
        writable=False,
        readable=True,
        resolve_path=True,
        allow_dash=False,
        path_type=None,
    ),
    default=def_home,
    envvar='SSTACK_PATH',
    show_default=True,
    help=help_path,
)
def showmod2(path: str):
    droot = ensure_droot(path)
    droot.vis_modules()
    print()

# sstack modules regen
@modules.command('regen', help='Regenerate stack module file.')
@click.option('--path', '-p',
    type=click.Path(
        exists=True,
        file_okay=False,
        dir_okay=True,
        writable=True,
        readable=True,
        resolve_path=True,
        allow_dash=False,
        path_type=None,
    ),
    default=def_home,
    envvar='SSTACK_PATH',
    show_default=True,
    help=help_path,
)
@click.option('--type', '-t',
    required=True,
    envvar='SSTACK_TYPE',
    help=help_type,
)
@click.option('--name', '-n',
    required=True,
    envvar='SSTACK_NAME',
    help=help_name,
)
def module_regen(path: str, type: str, name: str):
    droot = ensure_droot(path)

    # Validate Stack Exists
    if droot.val_stack(name, type):
        stack: sstack_stack = droot.get_stack(name, type)
    else:
        print('Stack "{}", of type "{}", does not exist! Stopping'.format(name,type))
        exit(5)

    # Regen Module FIle
    globals()[stack.type].generate_module(stack)
    print('{}/{}: Sucessfully Regenerated Module File!'.format(type,name))

    # Report Success
    print()
    stack.vis()
    print()
    print('Module Setup/Load Commands:')
    print()
    print('module use "{}"'.format(droot.mdir))
    print('module load {}/{}'.format(stack.type, stack.name))
    print()

# sstack modules default show
@modules_default.command('show', help='Show stack module files.')
@click.option('--path', '-p',
    type=click.Path(
        exists=True,
        file_okay=False,
        dir_okay=True,
        writable=False,
        readable=True,
        resolve_path=True,
        allow_dash=False,
        path_type=None,
    ),
    default=def_home,
    envvar='SSTACK_PATH',
    show_default=True,
    help=help_path,
)
def showdefmod(path: str):
    droot = ensure_droot(path)

    for path in pathlib.Path(droot.mdir).rglob('default'):
        type = pathlib.Path(path).parent.stem

        if pathlib.Path(path).is_symlink:
            name = pathlib.Path(path).resolve()
            bname = pathlib.Path(name).stem
            print('"{}" default module override = "{}/{}"'.format(type,type,bname))
        else:
            print('"{}" has a non symlink default file. Skipping.'.format(type))

# sstack modules default set
@modules_default.command('set', help='Set default module for a specific stack type.')
@click.option('--path', '-p',
    type=click.Path(
        exists=True,
        file_okay=False,
        dir_okay=True,
        writable=True,
        readable=True,
        resolve_path=True,
        allow_dash=False,
        path_type=None,
    ),
    default=def_home,
    envvar='SSTACK_PATH',
    show_default=True,
    help=help_path,
)
@click.option('--type', '-t',
    required=True,
    envvar='SSTACK_TYPE',
    help=help_type,
)
@click.option('--name', '-n',
    required=True,
    envvar='SSTACK_NAME',
    help=help_name,
)
def set_defmod(path: str, type: str, name: str):
    droot = ensure_droot(path)

    # Validate Stack Exists
    if droot.val_stack(name, type):
        stack: sstack_stack = droot.get_stack(name, type)
    else:
        print('Stack "{}", of type "{}", does not exist! Stopping'.format(name,type))
        exit(5)

    # Validate Module File Exists
    if not stack.val_module():
        print('The module file "{}" for Stack "{}", of type "{}", does not exist! Stopping!!'.format(stack.module,name,type))
        exit(6)

    # Set Symlink Path
    sym_path = pathlib.Path('{}/{}/default'.format(droot.mdir,type))

    # Validate Existing Symlink
    if sym_path.is_symlink():
        sym_dest = pathlib.Path(sym_path.resolve())
        question='Do you want to replace the existing default of "{}/{}" with "{}/{}"?'.format(type,sym_dest.stem,type,name)
        if query_yes_no(question, default="no"):
            sym_path.unlink()
        else:
            exit(7)
    elif sym_path.is_file():
        sym_path.unlink()
    elif sym_path.is_dir():
        shutil.rmtree(sym_path)

    # Create Symlink
    sym_path.symlink_to(stack.module)
    print('Default Module Set Successfully!')

# sstack modules default reset
@modules_default.command('reset', help='Remove the symlink default module override.')
@click.option('--path', '-p',
    type=click.Path(
        exists=True,
        file_okay=False,
        dir_okay=True,
        writable=True,
        readable=True,
        resolve_path=True,
        allow_dash=False,
        path_type=None,
    ),
    default=def_home,
    envvar='SSTACK_PATH',
    show_default=True,
    help=help_path,
)
@click.option('--type', '-t',
    required=True,
    envvar='SSTACK_TYPE',
    help=help_type,
)
def reset_defmod(path: str, type: str):
    droot = ensure_droot(path)

    # Set Symlink Path
    sym_path = pathlib.Path('{}/{}/default'.format(droot.mdir,type))

    # Validate Existing Symlink
    if sym_path.is_symlink():
        sym_dest = pathlib.Path(sym_path.resolve())
        question='Remove default of "{}/{}" for type "{}"?'.format(type,sym_dest.stem,type)
        if query_yes_no(question, default="no"):
            sym_path.unlink()
            print('Default override removed successfully!')
        else:
            exit(8)
    elif sym_path.is_file():
        sym_path.unlink()
        print('"default" should not be of type "file"! Removed!!')
    elif sym_path.is_dir():
        shutil.rmtree(sym_path)
        print('"default" should not be of type "directory"! Removed!!')
    else:
        print('No default module override, using "default" symlink, found!')

if __name__ == "__main__":
    cli()
