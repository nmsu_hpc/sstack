# SStack Contribution Guide

## Site Specific Configuration

Right now this project does not directly support site specific configuration. If this is required the best option is to fork this project and make your site specific changes there.

## Dev Specific Download and Install

Refer to readme for prerequisites

```bash
# Download to desired location.
git clone https://gitlab.nmsu.edu/hpc/sstack

# Create python virtual environment and install sstack
cd sstack
make build-dev

# Source environment setup script (assumes bash)
source share/setup-env.sh

# Start using sstack
sstack --help
```

There are 2 main build modes `make build-dev` and `make build-prod`.  Dev build mode is preferred when working on the code base as it will reflect changes to the source code in real time without having to rebuild.

For a list of all make commands run `make` or `make help`.

Notes:

- On fast moving linux distributions, like Fedora, it is common for updates to python to break previously installed virtual environments as they link back to the python binary that created them.
  - For example Fedora may move from Python 3.10 to Python 3.11. If sstack is installed per the above method it is likely to break until you run `make clean` then rebuild it.
- Be sure to test changes against the oldest supported python listed in the README.md.
