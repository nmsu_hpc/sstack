#!/bin/bash

# Stop On Error
set -eo pipefail

# Set Basic Global Vars
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
PRJ_DIR="$(dirname "$(dirname "$SCRIPT_DIR")")"

# Ensure Working Directory
if [ "$(pwd)" != "$PRJ_DIR" ]; then
	cd "$PRJ_DIR" || exit 15
fi

# Ensure Pre-Commit Is Installed
pre-commit install --install-hooks > /dev/null

# Run Pre Commit
if [ "$1" == "all" ]; then
	pre-commit run --all-files
elif [ "$1" == "ci" ]; then
	# Fetching only up to the closest merge-base to save time
	git fetch --depth 1 origin "$CI_MERGE_REQUEST_DIFF_BASE_SHA"

	# Diff only changed files within MR
	readarray -t array < <(git diff-tree --name-only -r "$CI_MERGE_REQUEST_DIFF_BASE_SHA" "$CI_COMMIT_SHA")

	pre-commit run --files "${array[@]}"
elif [ "$1" == "branch" ]; then
	# Update  Origin main/master branch
	git fetch origin master

	# Git Current Working Local Branch
	branch="$(git symbolic-ref --short -q HEAD)"

	# Diff only changed files within branch
	readarray -t array < <(git diff --name-only "$branch" "$(git merge-base "$branch" "origin/master")")

	pre-commit run --files "${array[@]}"
else
	echo "Do you want to stage all changes?"
	echo "Lint only works on staged changes!"
	read -rp "(y/n): " stage
	if [ "$stage" == "y" ]; then
		git add --all
	fi

	pre-commit run
fi
