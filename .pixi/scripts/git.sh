#!/usr/bin/env bash

# Stop On Error
set -eo pipefail

# Set Basic Global Vars
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
PRJ_DIR="$(dirname "$(dirname "$SCRIPT_DIR")")"
#CRED='\033[0;31m'
CGREEN='\033[0;32m'
CRESET='\033[0m'

# Ensure Working Directory
if [ "$(pwd)" != "$PRJ_DIR" ]; then
	cd "$PRJ_DIR" || exit 15
fi

# Tasks defined in arg1/$1
if [ "$1" == "hooks" ]; then
	pre-commit install --install-hooks

elif [ "$1" == "cleanup" ]; then
	git checkout main
	git fetch --prune
	git pull --autostash

	# Remove Squashed+Merged Branches
	python3 -m git_delete_merged_branches --effort 3 --branch main --remote origin --yes

elif [ "$1" == "pushnew" ]; then
	git push --set-upstream origin "$(git branch  --no-color  | grep -E '^\*' | awk '{print $2}')"

elif [ "$1" == "userlist" ]; then
	echo
	echo
	echo "Please review your git user configuration!"
	echo
	echo "User (Global)=$(git config --global user.name)"
	echo "Email (Global)=$(git config --global user.email)"
	echo
	echo "User (Local)=$(git config --local user.name)"
	echo "Email (Local)=$(git config --local user.email)"

elif [ "$1" == "userset" ]; then
	read -rp "Git User: " name
	read -rp "Git Email: " email
	echo

	flag=""
	prompt="Please select a configuration realm:"
	PS3="$prompt "
	select opt in local global; do
		if (( REPLY > 0 && REPLY <= 2 )) ; then
			flag="--$opt"
			break
		else
			echo "Invalid option. Try another one." >&2
		fi
	done

	git config $flag user.name "$name"
	git config $flag user.email "$email"

	echo "Git user information for the local repo is set."
	echo "User ($flag)=$(git config $flag user.name)"
	echo "Email ($flag)=$(git config $flag user.email)"

elif [ "$1" == "merge" ]; then
	branch="$(git rev-parse --abbrev-ref HEAD | sed 's|/|%2F|g')"
	echo ""
	echo "Review Existing Merge Requests:"
	echo -e "${CGREEN}https://gitlab.nmsu.edu/hpc/sstack/-/merge_requests${CRESET}"
	echo ""
	echo "New Merge Request:"
	echo -e "${CGREEN}https://gitlab.nmsu.edu/hpc/sstack/-/merge_requests/new?merge_request%5Bsource_branch%5D=${branch}${CRESET}"
	echo ""
else
	echo "Argument Required!"
fi
