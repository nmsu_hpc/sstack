#!/usr/bin/env bash

# Stop On Error
set -e

# Set Basic Global Vars
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
PRJ_DIR="$(dirname "$(dirname "$SCRIPT_DIR")")"
CRED='\033[0;31m'
CGREEN='\033[0;32m'
CRESET='\033[0m'

# Create Free Port Finding Function
function EPHEMERAL_PORT() {
	low_bound=49152
	range=16384
	while true; do
		candidate=$(( low_bound + (RANDOM % range) ))
		if ! (echo "" >/dev/tcp/127.0.0.1/${candidate}) &>/dev/null ; then
			echo $candidate
			break
		fi
	done
}

# Ensure Working Directory
if [ "$(pwd)" != "$PRJ_DIR" ]; then
	cd "$PRJ_DIR" || exit 15
fi

# Setup code-server Directory
data_dir="$PRJ_DIR/.code-server"
mkdir -p "$data_dir/extensions"

# Set Config File
CODE_SERVER_CONFIG="$data_dir/config.yaml"

# Create Config File If It Doesn't Exist
if  [ ! -f "$CODE_SERVER_CONFIG" ]; then
	# Set Bind Address With Free Port
	free_port="$(EPHEMERAL_PORT)"
	if hostname -s | grep -qi discovery ; then
		CODE_BIND_ADDR="0.0.0.0:${free_port}"
	else
		CODE_BIND_ADDR="127.0.0.1:${free_port}"
	fi

	# Generate Password
	CODE_PASSWORD="$(tr -dc A-Za-z0-9 </dev/urandom | head -c 25)"

	# Create Config File
	cat <<- EOF > "$CODE_SERVER_CONFIG"
		auth: password
		password: $CODE_PASSWORD
		bind-addr: $CODE_BIND_ADDR
		cert: false
		user-data-dir: $data_dir
		extensions-dir: $data_dir/extensions
		disable-telemetry: true
		disable-update-check: true
		disable-workspace-trust: true
	EOF
fi

# Get Needed Config File Settings
pw="$(grep '^password' "$CODE_SERVER_CONFIG" | awk -F':' '{print $2}' | tr -d '[:space:]')"
port="$(grep '^bind-addr' "$CODE_SERVER_CONFIG" | awk -F':' '{print $3}' | tr -d '[:space:]')"

# Update Port if in use
if (echo "" >"/dev/tcp/127.0.0.1/${port}") &>/dev/null ; then
        free_port="$(EPHEMERAL_PORT)"
        sed -i "s/$port/$free_port/" "$CODE_SERVER_CONFIG"
        port="$free_port"
fi

# Print Help Text
echo "Access Server Via:"
if hostname -s | grep -qi discovery ; then
        echo -e "${CGREEN}https://ondemand.nmsu.edu/rnode/$(hostname -s)-ib0.cluster.local/${port}/${CRESET}"
else
        echo -e "${CGREEN}http://localhost:${port}/${CRESET}"
fi
echo -e "Password = ${CRED}${pw}${CRESET}"
echo ""
echo "Press Control+C To Stop Server..."
echo ""

# Handle Rogue Env Vars
unset port
unset PORT
unset password
unset PASSWORD

# Handle Other Rogue Env Vars
VSCODE_EXTENSIONS="$data_dir/extensions"
export VSCODE_EXTENSIONS

# Start Server
code-server \
        --config="$CODE_SERVER_CONFIG" \
        --ignore-last-opened \
        "$PRJ_DIR" &>"$data_dir/server.log"
