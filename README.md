# Software Stacks (sstack)

sstack is a tool to install multiple software stacks, such as Spack, EasyBuild, and Conda. These stacks are then linked together, using lmod module files, to easily integrate with most HPC environments.

The primary goal is to provide an easy way to install common software build tools, with sane or site specific defaults, for both site admins and end users. The secondary goal is to provide a structured Lmod module hierarchy to tie together all software on a HPC cluster.

End result is that sstack should be used by site admins to install software in global locations and by end users to install software in their home/project directories. Since the module hierarchy is identical in both locations the over all module path can remain highly structured.

## Table of Contents

- [Software Stacks (sstack)](#software-stacks-sstack)
  - [Table of Contents](#table-of-contents)
  - [Important Terminology](#important-terminology)
  - [Download and Install](#download-and-install)
  - [Commands](#commands)
  - [Environment Variables](#environment-variables)
  - [Stack Types](#stack-types)
  - [Example (Conda)](#example-conda)
  - [Example (Spack)](#example-spack)
  - [Example (Default Modules)](#example-default-modules)

## Important Terminology

1. SStack
   1. name of this tool.
2. Stack
   1. Single instance, or installation, of a stack type (software tool).
3. Stack Type
   1. Specific software tool deployed in stack (spack, conda, easybuild, etc..).
4. Module
   1. This is a lua file used by the Lmod Module System. Every stack has its own module that is placed in a common module tree.
5. Module Tree
   1. A collection of modules stored in the same directory or set of subdirectories under a common directory.
   2. Each SStack data root has a single module tree it organizes everything under.
6. SStack Data Root or Path (-p / --path option)
   1. This is the top level directory where SStack stores stacks, modules, and a statefull json database.

```less
data_root (Default: ~/sstack/osID_majorVer)
├── modules
│   └── stack_type
│       └── stack_name.lua
├── stacks
│   └── stack_type
│       └── stack_name
│           ├── stack_installation_data
│           └── ...
└── stacks.json
```

```console
$ tree ~/sstack
/home/username/sstack
├── modules
│   └── custom
│       └── c1.lua
├── stacks
│   └── custom
│       └── c1
│           ├── builds
│           ├── modules
│           ├── packages
│           └── sources
└── stacks.json
```

## Download and Install

Prerequisites:

- bzip2
- curl
- git
- Lmod >= 8.2.7
- make
- tar
- Writable/Executable '/tmp' and '$HOME' directories

To install SStack it is reccommended to install it as a stack controlled by sstack. This will create a SStack data root and install a stack of the type sstack there. The upside to this method is that it integrates SStack with the module system directly without having to setup multiple module paths. A helper script is provided for this.

```bash
# Install SStack (Install to default directory in ~/sstack)
curl -fsSL "https://gitlab.com/nmsu_hpc/sstack/-/raw/main/share/install_sstack_latest.sh" | bash -s --

# Add new sstack module tree to module path
# Assuming os is Fedora 36
module use ~/sstack/fedora_36/modules

# Load SStack
module load sstack

# Use SStack
sstack --help
```

You can also provide additional arguments. The first argument controls where to install to and the second controls the stack/module name to label the sstack stack.

```bash
# Install to custom directory
curl -fsSL "https://gitlab.com/nmsu_hpc/sstack/-/raw/main/share/install_sstack_latest.sh" | bash -s -- "/software/sstack"

# Install to custom directory and set custom stack name
## This can be used to reinstall a broken sstack installation
curl -fsSL "https://gitlab.com/nmsu_hpc/sstack/-/raw/main/share/install_sstack_latest.sh" | bash -s -- "/software/sstack" "latest-el7"
```

Notes:

- We currently do not branch or tag releases and will not likely do so until we have to introduce breaking changes. For now just follow the latest available on the main branch.

## Commands

SStack includes an extensive help system. Add `--help` to any command to see possible options, arguments, or subcommands.

```console
$ sstack --help
Usage: sstack [OPTIONS] COMMAND [ARGS]...

Options:
  --version  Show the version and exit.
  --help     Show this message and exit.

Commands:
  install  Install new software stack.
  modules  Manage installed module files.
  remove   Remove existing software stack.
  show     List installed stacks, available types, etc..
  update   Update software stack and regenerate modulefile.
```

```console
$ sstack show --help
Usage: sstack show [OPTIONS] COMMAND [ARGS]...

  List installed stacks, available types, etc..

Options:
  --help  Show this message and exit.

Commands:
  modules  Show stack modules.
  stacks   Show installed software stacks.
  types    Show available stack types.
```

```console
$ sstack show stacks --help
Usage: sstack show stacks [OPTIONS]

  Show installed software stacks.

Options:
  -p, --path DIRECTORY  Location of stacks and module files.  [default:
                        /home/username/sstack/fedora_36]
  --help                Show this message and exit.
```

## Environment Variables

A few environmental variables are supported that can be used in-place of options such as `--path`. The order of precendence is command option, environment variable, and then the default value (should there be a default).

- `-p` or `--path` = `SSTACK_PATH`
- `-t` or `--type` = `SSTACK_TYPE`
- `-n` or `--name` = `SSTACK_NAME`

Below is an example of doing the same set of commands 3 different ways to acheive an identical effect.

```bash
# No Env
sstack install -p /software/sstack -t spack -n 2022A-el7
sstack show stacks -p /software/sstack

# In-Line Env
SSTACK_PATH=/software/sstack sstack install -t spack -n 2022A-el7
SSTACK_PATH=/software/sstack sstack show stacks

# Multi-Line Env
export SSTACK_PATH="/software/sstack"
sstack install -t spack -n 2022A-el7
sstack show stacks

```

## Stack Types

Supported Stack Types:

- [conda](https://docs.conda.io/projects/conda/en/latest/index.html)
  - Conda is an open source package management and environment management system. This stack delivers a custom anaconda/miniconda type deployment based on the communities 'conda-forge' channel with 'conda'/'mamba' as the package manager.
  - Due to licensing the default package channel is set to [conda-forge](https://conda-forge.org/).
    - If you wish to use Anaconda licensed package channels you will specifically need to opt-in. Please verify you have proper [licensing](https://www.anaconda.com/pricing) or meet their free use criteria first!
  - Additional Dependencies:
    - None beyond the standard SStack prerequisites.
- custom
  - Custom Hand Built Packages.
  - Additional Dependencies:
    - None beyond the standard SStack prerequisites though depending on what users are expected to be building by hand you may want to provide common build tools like GCC, GNU MAKE, etc...
- [easybuild](https://docs.easybuild.io/en/latest/index.html)
  - EasyBuild is a software build and installation framework that allows you to manage (scientific) software on High Performance Computing (HPC) systems in an efficient way.
  - [Additional Dependencies](https://docs.easybuild.io/en/latest/Installation.html#dependencies):
    - C/C++ compiler such as gcc/g++
    - Other common shell utilities, refer to link above.
- [micromamba](https://mamba.readthedocs.io/en/latest/user_guide/micromamba.html)
  - Micromamba is a standalone version of Mamba which is an alternative to Conda. This stack delivers a custom anaconda/miniconda type deployment based on the communities 'conda-forge' channel with 'micromamba' as the package manager.
  - Due to licensing the default package channel is set to [conda-forge](https://conda-forge.org/).
    - If you wish to use Anaconda licensed package channels you will specifically need to opt-in. Please verify you have proper [licensing](https://www.anaconda.com/pricing) or meet their free use criteria first!
  - Additional Dependencies:
    - None beyond the standard SStack prerequisites.
- [nix](https://nixos.org/)
  - Nix, the reproducable, declarative, and reliable package manager, but containarized for portability.
  - Additional Dependencies:
    - [Apptainer](https://apptainer.org/)
  - Notes
    - To get around limitations with nix and the required /nix directory, the entire thing has been containarized. '/nix' and other important files/folders in the user home directory are located in the stack directory but bind mounted to the correct locations inside the container.
    - Binary wrappers (extenstionless shell scripts that run the container with correct mounts) are provided for all nix commands and code-server.
    - A script 'sif_launch.sh' is provided to run arbatrary commands inside the container with all the correct bind mounts.
      - Run a command inside container:
        - `sif_launch.sh cat /etc/os-release`
        - `sif_launch.sh bash`
      - Install and Run saga-gis:
        - `nix-env -iA nixpkgs.saga`
        - `sif_launch.sh saga_cmd --version`
      - Use a nix-shell env non-interactively:
        - `nix-shell -p neofetch --run neofetch`
- [shpc](https://singularity-hpc.readthedocs.io/en/latest/)
  - Singularity Registry HPC (shpc) allows you to install containers as modules.
  - Additional Dependencies:
    - [Apptainer](https://apptainer.org/) or [Sylabs Singularity](https://sylabs.io/singularity/)
- [sstack](https://gitlab.com/nmsu_hpc/sstack)
  - This tool...
  - sstack is a tool to install multiple software stacks, such as Spack and Anaconda (miniforge/micromamba). These stacks are then linked together, using lmod module files, to easily integrate with most HPC environments.
  - Additional Dependencies:
    - Refer to the prerequisites listed earlier in this document.
- [pkgsrc](https://www.pkgsrc.org/)
  - pkgsrc is a framework for managing third-party software on UNIX-like systems, currently containing over 26,000 packages. It is the default package manager of NetBSD and SmartOS, and can be used to enable freely available software to be built easily on a large number of other UNIX-like platforms.
  - Additional Dependencies:
    - None beyond the standard SStack prerequisites. pkgsrc often requires essential system tools and utilities like compilers (GCC, Clang), make, tar, gzip, etc.
- [pixi](https://pixi.sh/)
  - pixi is a fast software package manager built on top of the existing conda ecosystem. Spins up development environments quickly on Windows, macOS and Linux. Automatic lockfiles produce reproducible environments across operating systems (without Docker!). pixi supports Python, R, C/C++, Rust, Ruby, and many other languages.
  - Additional Dependencies:
    - None beyond the standard SStack prerequisites.

SStack does not currently validate dependencies so it is up to the user to ensure the the listed dependencies exist and are accessible.

## Example (Conda)

```console
$ module use ~/sstack/modules
$ module load sstack
$ sstack install --name test0 --type conda
...
...
...
Stack Successfully Installed!
+-------+-------+---------+------------------------------------------+
| Name  | Type  | Version | Path                                     |
+-------+-------+---------+------------------------------------------+
| test0 | conda | 4.13.0  | /home/username/sstack/stacks/conda/test0 |
+-------+-------+---------+------------------------------------------+

Module Setup/Load Commands:

module use "/home/username/sstack/modules"
module load conda/test0

$ module load conda/test0
(base)$ conda --version
conda 4.13.0

(base)$ which python
~/sstack/stacks/conda/test0/bin/python

(base)$ conda create --name tester python ipykernel
...
...
...
Preparing transaction: done
Verifying transaction: done
Executing transaction: done
#
# To activate this environment, use
#
#     $ conda activate tester
#
# To deactivate an active environment, use
#
#     $ conda deactivate

(base)$ conda activate tester
(tester)$ which python
~/sstack/stacks/conda/test0/envs/tester/bin/python
```

## Example (Spack)

```console
$ module use ~/sstack/modules
$ module load sstack
$ sstack install --name test1 --type spack
...
...
...
Stack Successfully Installed!
+-------+-------+---------+------------------------------------------+
| Name  | Type  | Version | Path                                     |
+-------+-------+---------+------------------------------------------+
| test1 | spack | 0.18.0  | /home/username/sstack/stacks/spack/test1 |
+-------+-------+---------+------------------------------------------+

Module Setup/Load Commands:

module use "/home/username/sstack/modules"
module load spack/test1

$ module load spack/test1
$ spack --version
0.18.0 (c09bf37ff690c29779a342670cf8a171ad1b9233)

$ spack install zlib
[+] /home/username/sstack/stacks/spack/test1/opt/spack/linux-fedora35-x86_64_v3/gcc-11.3.1/zlib-1.2.12-ojxmrh7kuyqszihdl5573b4pjadftkld

$ module spider zlib
------------------------------------------------------------------------------------------------
  zlib: zlib/1.2.12-ojxmrh7
------------------------------------------------------------------------------------------------

    You will need to load all module(s) on any one of the lines below before the "zlib/1.2.12-ojxmrh
7" module is available to load.

      spack/test1

    Help:
      A free, general-purpose, legally unencumbered lossless data-compression
      library.
```

## Example (Default Modules)

Below is an example on how to set Lmod default module overrides to control the default module used on module loads (`module load custom` instead of `module load custom/c1`).

Install test stacks:

```bash
sstack install -t custom -n c1
sstack install -t custom -n c2
```

Show current default based on highest version:

```console
$ module avail
...
...
----- /home/username/sstack/modules -----
   custom/c1    custom/c2 (D)
...
...
  Where:
   D:  Default Module
...
...
```

Set older version as the default module:

```console
$ sstack modules default set -t custom -n c1
Default Module Set Successfully!
```

Show the changes in a few different ways:

```console
$ module avail
...
...
----- /home/username/sstack/modules -----
   custom/c1 (D)   custom/c2
...
...
  Where:
   D:  Default Module
...
...

$ sstack show modules
Module Tree (/home/username/sstack/modules):

modules/
|-- custom/
|   |-- c1.lua
|   |-- c2.lua
|   |-- default -> /home/username/sstack/modules/custom/c1.lua

Notes:
- "default ->" represent default module overrides using symlinks.
  - Destination becomes default module for that stack type.
  - To modify override see "sstack modules default --help".

$ sstack modules default show
"custom" default module override = "custom/c1"
```

Reset/Remove module default override:

```console
$ sstack modules default reset -t custom
Remove default of "custom/c1" for type "custom"? [y/N] y
Default override removed successfully!

$ module avail
...
...
----- /home/username/sstack/modules -----
   custom/c1    custom/c2 (D)
...
...
  Where:
   D:  Default Module
...
...
```
