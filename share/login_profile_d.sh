#!/bin/bash

# /etc/profile.d/lmod.sh

# The is an example script for setting up lmod on user login.

# If Module Path Exists Do Nothing (Except Default EPEL 00-modulepath.sh)
if [ -n "$MODULEPATH" ] && [ "$MODULEPATH" != "/etc/modulefiles:/usr/share/modulefiles" ]; then
	return
fi

# Custom LMOD Settings
export LMOD_ADMIN_FILE="/usr/share/lmod/admin.list"
export LMOD_PAGER="less"
export LMOD_AUTO_SWAP="no"
export LMOD_DISABLE_NAME_AUTOSWAP='yes'
export LMOD_CACHED_LOADS="yes"
export LMOD_CASE_INDEPENDENT_SORTING="yes"
export LMOD_COLORIZE="yes"
export LMOD_DISABLE_NAME_AUTOSWAP="yes"
export LMOD_EXACT_MATCH="no"
export LMOD_MPATH_AVAIL="yes"
export LMOD_PIN_VERSIONS="yes"
export LMOD_PREPEND_BLOCK="normal"
if [ -f "/usr/share/lmod/rc.lua" ]; then
	export LMOD_RC="/usr/share/lmod/rc.lua"
fi
export LMOD_SETTARG_CMD=":"
export LMOD_FULL_SETTARG_SUPPORT="yes"
export LMOD_SETTARG_FULL_SUPPORT="yes"
export LMOD_SETTARG_TITLE_BAR="yes"
export LMOD_SYSTEM_DEFAULT_MODULES="sstack/latest-el7"

# Setup Module Path
MODULEPATH=""

## GLobal Modules
if [ -d "/software/sstack/modules" ]; then
	MODULEPATH="/software/sstack/modules:${MODULEPATH}"
fi

## GLobal Test Modules
if [ "$USER" == "pkgmgr" ] && [ -d "/software/sstack_test/modules" ]; then
	MODULEPATH="/software/sstack_test/modules:${MODULEPATH}"
fi

## User Modules
if [ -d "$HOME/sstack/modules" ]; then
	MODULEPATH="$HOME/sstack/modules:${MODULEPATH}"
fi

export MODULEPATH
