help([[For help contact your site administrators.]])
whatis("Name : sstack")
whatis("Version: latest")
whatis("Target: x86_64")
whatis("Short description: sstack is a tool to install multiple software stacks, such as Spack, EasyBuild, and Conda. These stacks are then linked together, using lmod module files, to easily integrate with most HPC environments.")

family("sstack")

local base = ""
local share = pathJoin(base, 'share')

-- sh,dash,bash,zsh,csh,tcsh,ksh
local myShell = myShellName()

setenv("SSTACK_ROOT",base)
prepend_path("PATH",pathJoin(base, 'bin'))

-- Source Environment Setup Scripts
if (myShell == "bash") then
    -- Load/Unload Commands
    if (mode() == "load") then
        local cmd = 'source ' .. pathJoin(share,'setup-env.sh')
        execute{cmd=cmd, modeA={"load"}}
    elseif (mode() == "unload") then
        local cmd = 'source ' .. pathJoin(share,'unsetup-env.sh')
        execute{cmd=cmd, modeA={"unload"}}
    end
end
