#!/usr/bin/env bash

SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
SSTACK_ROOT="$(dirname "$SCRIPT_DIR")"
bdir="$SSTACK_ROOT/bin"

# Setup Path
if ! [[ "$PATH" =~ (^|:)"${bdir}"(:|$) ]]; then
	PATH="${bdir}:$PATH"
	export PATH
fi

# Setup Completions (Bash >= 4.4 required)
if [ "$(echo "$BASH_VERSION" | awk -F'.' '{print $1}')" -eq 4 ]; then
	if [ "$(echo "$BASH_VERSION" | awk -F'.' '{print $2}')" -ge 4 ]; then
		eval "$(sstack completion bash)"
	fi
elif [ "$(echo "$BASH_VERSION" | awk -F'.' '{print $1}')" -ge 5 ]; then
	eval "$(sstack completion bash)"
fi

# Cleanup
unset SCRIPT_DIR
unset bdir
