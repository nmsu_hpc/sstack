#!/usr/bin/env bash

SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
SSTACK_ROOT="$(dirname "$SCRIPT_DIR")"


# Cleanup Path
PATH="$(echo "$PATH" | tr ':' '\n' | grep -Exv "^\$|^${SSTACK_ROOT}\$|^${SSTACK_ROOT}/.*\$" | paste -sd ':' -)"
export PATH

# Cleanup Functions
unset -f "_sstack_completion"
unset -f "_sstack_completion_setup"

# Cleanup Vars
unset SCRIPT_DIR
unset SSTACK_ROOT
