#!/usr/bin/env bash

## Usage (Install to default directory in ~/sstack):
## $ curl -fsSL "https://gitlab.com/nmsu_hpc/sstack/-/raw/main/share/install_sstack_latest.sh" | bash -s --
## or (Install to custom directory)
## $ curl -fsSL "https://gitlab.com/nmsu_hpc/sstack/-/raw/main/share/install_sstack_latest.sh" | bash -s -- "/software/sstack"
## or (Install to custom directory and set custom stack name)
## $ curl -fsSL "https://gitlab.com/nmsu_hpc/sstack/-/raw/main/share/install_sstack_latest.sh" | bash -s -- "/software/sstack" "latest-el7"

# Stop On Error
set -e

# Get Arguments (optional)(it's ok if it already exists.)
sstack_data_root="$1"
sstack_stack_name="$2"
sstack_update="$3"

# Setup Temp Directory
TMP_DIR="$(mktemp -d)"

# Set Trap to cleanup on exit
function cleanup() {
	ecode=$?
	if [ $ecode -ne 0 ]; then
		echo "There was a deployment failure! Stopping!!"
		if [ "$sstack_update" == "update" ]; then
			echo "If the update failed due to git errors, try removing the 'update' argument. This will remove then reinstall the sstack stack instead of trying to update it in place."
		fi
	fi
	rm -rf "$TMP_DIR"
	exit $ecode
}
trap cleanup EXIT

# Git Clone sstack
git clone "https://gitlab.com/nmsu_hpc/sstack.git" "$TMP_DIR"

# Move To Install Directory
cd "$TMP_DIR"

# Run Make
make build-prod

# Generate/Set Stack Name
if [ -z "$sstack_stack_name" ]; then
	os_ver="$(grep -i '^VERSION_ID' /etc/os-release | awk -F'=' '{print $2}' | sed 's/"//g' | tr '[:upper:]' '[:lower:]')"
	os_name="$(grep -i '^NAME' /etc/os-release | awk -F'=' '{print $2}' | sed 's/"//g' | sed 's/ /_/g' | tr '[:upper:]' '[:lower:]')"
	sstack_stack_name="${os_name}-${os_ver}"
fi

if [ "$sstack_update" == "update" ]; then
	# Update Existing SStack Stack Deployment
	if [ -n "$sstack_data_root" ]; then
		./bin/sstack update -p "$sstack_data_root" -t sstack -n "$sstack_stack_name"
	else
		./bin/sstack update -t sstack -n "$sstack_stack_name"
	fi
else
	# Install SStack to data root
	if [ -n "$sstack_data_root" ]; then
		yes "y" | ./bin/sstack install -p "$sstack_data_root" -t sstack -n "$sstack_stack_name"
	else
		yes "y" | ./bin/sstack install -t sstack -n "$sstack_stack_name"
	fi
fi
