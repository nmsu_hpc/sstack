#!/usr/bin/env bash

# Source Git Completions
if [ -f "/usr/share/bash-completion/completions/git" ]; then
	source "/usr/share/bash-completion/completions/git"
fi
