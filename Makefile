.PHONY: $(MAKECMDGOALS)
SHELL := /bin/bash

export PRJ_DIR := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
export PIXI_HOME = $(PRJ_DIR)/.pixi
export PIXI_NO_PATH_UPDATE = 1
export PIXI_CACHE_DIR = $(PIXI_HOME)/cache
export PATH := $(PIXI_HOME)/bin:$(PATH)

ifndef VERBOSE
export MAKEFLAGS += --no-print-directory
endif

default: help

.pixi/bin/pixi:
	mkdir -p .pixi/bin
	bash -c "curl -fsSL https://pixi.sh/install.sh | bash"

#pixi.lock: pyproject.toml
#	pixi update

.pixi/envs/default: pixi.lock
	pixi install --frozen
	touch $@

.pixi/envs/dev: pixi.lock
	pixi install -e dev --locked
	pixi run -e dev pre-commit
	touch $@

## Build All Environemnts
build: .pixi/bin/pixi build-prod build-dev

## Build Produciton Environment
build-prod: .pixi/bin/pixi .pixi/envs/default

## Build Development Environment
build-dev: .pixi/bin/pixi .pixi/envs/dev

## Update Pixi Version and Pixi Packages (pixi.lock)
update: .pixi/bin/pixi update-pixi update-lock

update-lock: .pixi/bin/pixi
	pixi update

update-pixi: .pixi/bin/pixi
	pixi self-update --force

## Delete Pixi bin/cache/envs, Python cache, and legacy files/folders
clean:
	-rm -rf .pixi/bin
	-rm -rf .pixi/cache
	-rm -rf .pixi/envs
	-rm -rf .venv
	-rm -rf build
	-find ./sstack -depth -type d -name '__pycache__' -exec rm -rf {} \;

## Delete Pixi Environments
clean-envs:
	-rm -rf .pixi/envs
	-rm -rf .pixi/cache/uv-cache

## Run Shell in dev environment
shell: build-dev
	@echo "####"
	@echo "Run 'cd -' to return to project directory."
	@echo "####"
	pixi shell -e dev --locked

## Run VSCode code-server web server for development
vscode: build-dev
	pixi run -e dev --locked vscode

## Cleanup Merged+Deleted Branches
git-cleanup: .pixi/bin/pixi build-dev
	pixi run -e dev --locked git cleanup

## Push New Branch to Remote
git-push-new: .pixi/bin/pixi build-dev
	pixi run -e dev --locked git pushnew

## Show Git User Info (name/email)
git-user-list: .pixi/bin/pixi build-dev
	pixi run -e dev --locked git userlist

## Set Git User Info (name/email)
git-user-set: .pixi/bin/pixi build-dev
	pixi run -e dev --locked git userset

## Run Pre-Commit On Changed Files (uncommitted)
lint: .pixi/bin/pixi build-dev
	pixi run -e dev --locked lint

## Run Pre-Commit On Current Branch's Committed Changes
lint-branch: .pixi/bin/pixi build-dev
	pixi run -e dev --locked lint branch

## Run Pre-Commit On All Files
lint-all: .pixi/bin/pixi build-dev
	pixi run -e dev --locked lint all

lint-ci: .pixi/bin/pixi build-dev
	pixi run -e dev --locked lint ci

help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)";echo;sed -ne"/^## /{h;s/.*//;:d" -e"H;n;s/^## //;td" -e"s/:.*//;G;s/\\n## /---/;s/\\n/ /g;p;}" ${MAKEFILE_LIST}|LC_ALL='C' sort -f|awk -F --- -v n=$$(tput cols) -v i=19 -v a="$$(tput setaf 6)" -v z="$$(tput sgr0)" '{printf"%s%*s%s ",a,-i,$$1,z;m=split($$2,w," ");l=n-i;for(j=1;j<=m;j++){l-=length(w[j])+1;if(l<= 0){l=n-i-length(w[j])-1;printf"\n%*s ",-i," ";}printf"%s ",w[j];}printf"\n";}'|more $(shell test $(shell uname) == Darwin && echo '-Xr')
